#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <stdexcept>

using namespace std;

const char DELIMITER = ',';
const int POS_DELIMITER = 2;
const string ERROR = "Negatives not allowed: ";

int add(char* input);
bool getDelimeter(char *input);
vector<string> split(char* str, char delimiter);

int main(){
    char *test;
    // Test Step 1
    test = "1,2";
    cout << "Somma " << add(test) << endl;
    // Test Step 2
    test = "1,2,3";
    cout << "Somma " << add(test) << endl;
    // Test Step 3
    test = "1,\n2,3";
    cout << "Somma " << add(test) << endl;
    // Test Step 4
    test = "//;\n1;2;3";
    cout << "Somma " << add(test) << endl;
    // Test Step 6
    test = "1,2,3,3000";
    cout << "Somma " << add(test) << endl;
    // Test Step 5
    test = "1,2,-3,-4";
    cout << "Somma " << add(test) << endl;
    return 0;
}

int add(char* input) {
    vector<string> numbers;
    int sum = 0;
    if(getDelimeter(input) == 0){
        numbers = split(input, DELIMITER);       
    } else {
        numbers = split(input + (POS_DELIMITER + 1), *(input + POS_DELIMITER));
    }
    
    string err = "";
    int n;
    try {
        for (auto i = numbers.begin(); i != numbers.end(); ++i) {
            try {
                n = stoi(*i);
            } catch (invalid_argument &e){
                cout << "Invalid conversion" << endl;
                return 0;
            }
            if(n < 1000){
                if(n < 0) {
                    err += *i + ";"; 
                }
                sum += n;
            }   
        }
        if(err != "") {
            throw invalid_argument(ERROR + err);
        } 
    } catch (invalid_argument &e) {
        cout << e.what() << endl;
        return 0;
    }

    return sum;
}   

bool getDelimeter(char *input) {
    return (*input == '/' && *(input + 1) == '/');
}

vector<string> split(char* str, char delimeter) {
    vector<string> numbers;
    string number = "";
    while(*str != '\0') { 
        if(*str == delimeter) {
            numbers.push_back(number);
            number = "";
        } else if(*str != '\n') {
            number += *str;
        } 
        *str++;
    }
    numbers.push_back(number);
    
    return numbers;
}

