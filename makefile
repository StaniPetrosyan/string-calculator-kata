exec:
	make build
	./bin/main

build:
	mkdir -p bin
	g++ src/main.cpp -o bin/main

clean: 
	rm -r bin